import random, os, shutil
from glob import glob
from sklearn.model_selection import train_test_split

img_files = sorted(glob("dataset/city/train/image/ori/"+'*.jpg'))
mask_files = sorted(glob("dataset/city/train/GT/mask/"+'*.jpg'))

outputfile = "dataset/validation/"
seed = random.randrange(1,2000)

img, img_v, mask, mask_v = train_test_split(img_files, mask_files, test_size = 0.2, random_state =seed)
for imgV, maskV in zip(img_files, mask_files):
    print imgV
    print maskV
print "deone"
for imgV, maskV in zip(img_v, mask_v):
    print imgV
    print maskV
    shutil.move(imgV, "dataset/validation/frames/")
    shutil.move(maskV, "dataset/validation/mask/")
