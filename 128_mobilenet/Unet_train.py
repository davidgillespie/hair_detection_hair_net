from keras.models import Model
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras.optimizers import Adam
from keras.backend.tensorflow_backend import set_session
import tensorflow as tf
import keras
import cv2
import numpy as np
import os, errno
from glob import glob
import argparse
import random
import math
from keras.callbacks import TensorBoard
from keras import callbacks as cb
from keras import callbacks, optimizers
from Unet_keras import get_mob_unet_256, custom_objects
import callbacks
import random
import loss
import scipy.stats
from img_generators import load_train_data

tensorboard = TensorBoard(log_dir='./logs', histogram_freq=0,
                          write_graph=True, write_images=False)

class TrainModel:
    
    def __init__(self, flag):
        self.flag = flag

    def train_generator(self, image_generator, mask_generator):
        while True:
            yield(next(image_generator), next(mask_generator))

    def lr_step_decay(self, epoch):
        init_lr = self.flag.initial_learning_rate
        lr_decay = self.flag.learning_rate_decay_factor
        epoch_per_decay = self.flag.epoch_per_decay
        lrate = init_lr * math.pow(lr_decay, math.floor((1+epoch)/epoch_per_decay))
        print lrate
        return lrate

    def train_unet(self):
       
        img_size = self.flag.image_size
        batch_size = self.flag.batch_size
        epochs = self.flag.total_epoch
        img_path = self.flag.data_path
        lr = self.flag.initial_learning_rate
        seed = random.randrange(1, 1000)
        image_generator, mask_generator = load_train_data(img_path, img_size, batch_size, seed)


        config = tf.ConfigProto()
        # config.gpu_options.per_process_gpu_memory_fraction = 0.9
        config.gpu_options.allow_growth = True
        set_session(tf.Session(config=config))

        model = get_mob_unet_256(self.flag)
        model.compile(optimizer=Adam(lr=lr, decay=1e-6), loss=loss.dice_coef_loss, metrics=[loss.dice_coef, loss.recall, loss.precision, loss.pixelwise_binary_ce])
        
        print model.summary()
        
        if not os.path.exists(os.path.join(self.flag.ckpt_dir, self.flag.ckpt_name)):
            mkdir_p(os.path.join(self.flag.ckpt_dir, self.flag.ckpt_name))
        model_json = model.to_json()
        with open(os.path.join(self.flag.ckpt_dir, self.flag.ckpt_name, 'model.json'), 'w') as json_file:
            json_file.write(model_json)
        vis = callbacks.trainCheck(self.flag)
        
        model_checkpoint = ModelCheckpoint(os.path.join(self.flag.ckpt_dir, self.flag.ckpt_name,'weights.{epoch:03d}.h5'),period=5)
                    
        learning_rate = LearningRateScheduler(self.lr_step_decay)
        
        model.fit_generator(
            self.train_generator(image_generator, mask_generator),
            steps_per_epoch= image_generator.n // batch_size,
            epochs=epochs,
            callbacks=[model_checkpoint, learning_rate, vis, tensorboard]
        )


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: #Python > 2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else : raise
