from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.backend.tensorflow_backend import set_session
from keras.models import model_from_json
import tensorflow as tf
import keras
import cv2
import numpy as np
import os
from glob import glob
import argparse

def predict_image(flag):
    t_start = cv2.getTickCount()
    config = tf.ConfigProto()
    # config.gpu_options.per_process_gpu_memory_fraction = 0.9
    config.gpu_options.allow_growth = True
    set_session(tf.Session(config=config))

    with open(os.path.join(flag.ckpt_dir, flag.ckpt_name, 'model.json'), 'r') as json_file:
            loaded_model_json = json_file.read()
    model = model_from_json(loaded_model_json)
    weight_list = sorted(glob(os.path.join(flag.ckpt_dir, flag.ckpt_name, "weight*")))
    model.load_weights(weight_list[-1])
    print "[*] model load : %s"%weight_list[-1]
    t_total = (cv2.getTickCount() - t_start) / cv2.getTickFrequency() * 1000 
    print "[*] model loading Time: %.3f ms"%t_total
    print flag.test_image_path
    f = open("list.txt")
    for line in f:
        flag.test_image_path = line

        moo = "hair.jpg"
        line = line.strip('\n')
        line = line.strip('\t')
        print line

        print moo
        print moo
        imgInput = cv2.imread(line, 0)
        print line
        imgFore = cv2.imread(line, 0)
        print(imgInput.shape)
        imgInput = cv2.resize(imgInput, (128,128))
        input_data = imgInput.reshape((1,128,128,1))

        t_start = cv2.getTickCount()
        result = model.predict(input_data, 1)
        t_total = (cv2.getTickCount() - t_start) / cv2.getTickFrequency() * 1000
        print "Predict Time: %.3f ms"%t_total
    
        imgMask = (result[0]*255).astype(np.uint8)
        imgShow = cv2.cvtColor(imgInput, cv2.COLOR_GRAY2BGR)
        _, imgMask = cv2.threshold(imgMask, int(255*0.2), 255, cv2.THRESH_BINARY)
        imgMaskColor = cv2.applyColorMap(imgMask, cv2.COLORMAP_BONE)
        imgZero = np.zeros((256,256), np.uint8)
        # imgMaskColor = cv2.merge((imgZero, imgMask, imgMask))
        #imgShow = cv2.addWeighted(imgShow, 0.9, imgMaskColor, 0.3, 0.0)
        output_path = os.path.join(flag.output_dir, os.path.basename(line))
        #imgMaskColor = imgMaskColor.reshape(
        imgMask = cv2.resize(imgMask, (720, 1280))
        imgChanel = np.zeros((1280,720,4), np.uint8)
        imgChanel[:,:,0] = imgMask
        imgChanel[:,:,3] = imgMask
        imgChanel[:,:,1] = imgMask * 0.35294118
        
        imgChanel = cv2.GaussianBlur(imgChanel,(105,105),0)
        
        cv2.imwrite(output_path +".png", imgChanel)
        print "SAVE:[%s]"%output_path
        

    
