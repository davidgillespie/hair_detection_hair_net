import argparse
import re

import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from tensorflow.python.framework import graph_io
from tensorflow.python.framework.graph_util_impl import convert_variables_to_constants
import os
from keras.utils import CustomObjectScope

import loss
from keras.applications import mobilenet
from bilinearupsampling import BilinearUpSampling2D

num_output = 1

def custom_objects():
    return {
        'relu6': mobilenet.relu6,
        'DepthwiseConv2D': mobilenet.DepthwiseConv2D,
        'BilinearUpSampling2D': BilinearUpSampling2D,
        'dice_coef_loss': loss.dice_coef_loss,
        'dice_coef': loss.dice_coef,
        'recall': loss.recall,
        'pixelwise_binary_ce': loss.pixelwise_binary_ce,
        'precision': loss.precision,
}

def main(input_model):
    """
        Convert .h5 model to tensorflow pb file
    """
    out_put_path = os.path.dirname(input_model)
    out_put = re.sub(r"h5$", 'pb', os.path.basename(input_model));
    K.set_learning_phase(0)
    model = load_model(input_model, custom_objects = custom_objects())

    pred_nodes_names = ['output_%s' % n for n in range(num_output)]
    print('output nodes names are: ', pred_nodes_names)

    for idx, name in enumerate(pred_nodes_names):
        tf.identity(model.output[idx], name=name)

    sess=K.get_session()
    constant_graph = convert_variables_to_constants(sess, sess.graph.as_graph_def(), pred_nodes_names)
    graph_io.write_graph(constant_graph, out_put_path, out_put, as_text = False)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_model', type=str,)
    args, _ = parser.parse_known_args()
    main(**vars(args))

