from keras.models import Model
from keras.layers import Input, concatenate, Dense, ZeroPadding2D,GlobalAveragePooling2D, UpSampling2D, Convolution2D, Conv2D, MaxPooling2D, Conv2DTranspose, Activation, add, multiply, Reshape,Dropout
from keras.layers import BatchNormalization
from keras.optimizers import Adam, Adamax, SGD, Adagrad, Adadelta, RMSprop
from keras.applications import mobilenet
from keras.applications.mobilenet import DepthwiseConv2D, relu6
from keras.losses import binary_crossentropy
from keras import backend as K

from bilinearupsampling import BilinearUpSampling2D
import loss
import numpy as np

def custom_objects():
    return {
        'relu6': mobilenet.relu6,
        'DepthWiseConv2D': mobilenet.DepthwiseConv2D,
        'dice_coef_loss' : loss.dice_coef_loss,
        'dice_coef' : loss.dice_coef,
        'recall': loss.recall,
        'precision' : loss.precision
}

def BN(x):
    return BatchNormalization()(x)

def conv_block(input, filter, alpha, kernel = (3,3), strides = (1,1)):
    channel_axis = 1 if K.image_data_format() =='channels_first' else -1 # need to check this
    filters = int(filter*alpha)
    x = Conv2D(filters, (3,3), padding='same', use_bias=False, strides=strides)(input)
    x = BatchNormalization(axis = channel_axis)(x) # may change this order
    return  Activation(relu6)(x)

def depthwise_conv_block(inputs, pointwise_conv_filters, alpha, depth_multiplier=1, strides=(1,1)):
    """
        Adds a depthwise convolution block
    """
    channel_axis = 1 if K.image_data_format() =='channels_first' else -1 # need to check this
    pointwise_conv_filters = int(pointwise_conv_filters* alpha)
    x = DepthwiseConv2D((3,3), padding = 'same', depth_multiplier = depth_multiplier, strides = strides, use_bias = False)(inputs)
    x = BatchNormalization(axis = channel_axis)(x) # may change this order
    x = Activation(relu6)(x)

    x = Conv2D(pointwise_conv_filters, (1,1), padding='same', use_bias=False, strides=(1,1))(x)
    x = BatchNormalization(axis = channel_axis)(x) # may change this order
    x = Activation(relu6)(x)
    return x




def get_mob_unet_256(flag):
    img_rows = flag.image_size
    img_cols = flag.image_size
    alpha = flag.alpha
    alpha_up = flag.alpha_up
    print flag.alpha_up, flag.alpha
    depth_multiplier = flag.depth_multiplier
    
    inputs = Input((img_rows, img_cols, 3))
    
    b00 = conv_block(inputs, 32,alpha, strides=(2,2))
    b01 = depthwise_conv_block(b00, 64, alpha, depth_multiplier)
    
    b02 = depthwise_conv_block(b01, 128, alpha, depth_multiplier, strides = (2,2))
    b03 = depthwise_conv_block(b02, 128, alpha, depth_multiplier)
    
    b04 = depthwise_conv_block(b03, 256, alpha, depth_multiplier, strides = (2,2))
    b05 = depthwise_conv_block(b04, 256, alpha, depth_multiplier)
    
    b06 = depthwise_conv_block(b05, 512, alpha, depth_multiplier, strides = (2,2))
    b07 = depthwise_conv_block(b06, 512, alpha, depth_multiplier)
    b08 = depthwise_conv_block(b07, 512, alpha, depth_multiplier)
    b09 = depthwise_conv_block(b08, 512, alpha, depth_multiplier)
    b10 = depthwise_conv_block(b09, 512, alpha, depth_multiplier)
    b11 = depthwise_conv_block(b10, 512, alpha, depth_multiplier)
    
    b12 = depthwise_conv_block(b11, 1024, alpha, depth_multiplier, strides = (2,2))
    b13 = depthwise_conv_block(b12, 1024, alpha, depth_multiplier)
    filters = int(512 * alpha)
    up1 = concatenate([Conv2DTranspose(filters, (2,2), strides=(2,2), padding='same')(b13),b11], axis = 3)
    b14 = depthwise_conv_block(up1, filters, alpha_up, depth_multiplier)
    
    filters = int(256 * alpha)
    up2 = concatenate([Conv2DTranspose(filters, (2,2), strides=(2,2), padding='same')(b14),b05], axis = 3)
    b15 = depthwise_conv_block(up2, filters, alpha_up, depth_multiplier)
    
    filters = int(128 * alpha)
    up3 = concatenate([Conv2DTranspose(filters, (2,2), strides=(2,2), padding='same')(b15),b03], axis = 3)
    b16 = depthwise_conv_block(up3, filters, alpha_up, depth_multiplier)
    
    filters = int(64 * alpha)
    up4 = concatenate([Conv2DTranspose(filters, (2,2), strides=(2,2), padding='same')(b16),b01], axis = 3)
    b17 = depthwise_conv_block(up4, filters, alpha_up, depth_multiplier)
    
    filters = int(32 * alpha)
    up5 = concatenate([b17, b00], axis=3) #[Conv2DTranspose(filters, (2,2), strides=(2,2), padding='same')(b16),b01], axis = 3)
    
    b18 = conv_block(up5, filters, alpha_up)
    x = Conv2D(1,(1,1), kernel_initializer = 'he_normal', activation = 'linear')(b18)
    x = BilinearUpSampling2D(size=(2,2))(x)
    x= Activation('sigmoid')(x)
    
    model = Model(inputs=[inputs], outputs=[x])
    return model


