import coremltools
import argparse
import re
from keras.models import model_from_json
from keras.models import load_model
from keras.utils import CustomObjectScope
from hack import hack
import loss
from keras.applications import mobilenet
from bilinearupsampling import BilinearUpSampling2D
def custom_objects():
    return {
        'relu6': mobilenet.relu6,
        'DepthwiseConv2D': mobilenet.DepthwiseConv2D,
        'BilinearUpSampling2D': BilinearUpSampling2D,
        'dice_coef_loss': loss.dice_coef_loss,
        'dice_coef': loss.dice_coef,
        'recall': loss.recall,
        'pixelwise_binary_ce': loss.pixelwise_binary_ce,
        'precision': loss.precision,
}
    
def main(input_model):
    """
        Convert .h5 model to coreml Model
    """

    #json_file = open("checkpoint/unet/model.json", 'r')
    out_put = re.sub(r"h5$", 'mlmodel', input_model);
    hack()
    
    #loaded_model_json = json_file.read()
    

    with CustomObjectScope(custom_objects()):
        #model = model_from_json(loaded_model_json)
        model = load_model(input_model)
        coreml_model = coremltools.converters.keras.convert(model,
                                                                input_names='image',
                                                                image_input_names='image',
                                                                )
    coreml_model.save(out_put)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_model', type=str,)
    args, _ = parser.parse_known_args()

    main(**vars(args))

