from glob import glob
import os

import cv2
import scipy.stats
import argparse

from hair_color import solid_color, solid_gradient

def main(flag):
    
    img_files_jpg = sorted(glob(flag.image_path+'/*.jpg'))
    mask_files_jpg = sorted(glob(flag.mask_path+'/*.jpg'))
    
    img_files_png = sorted(glob(flag.image_path+'/*.png'))
    mask_files_png = sorted(glob(flag.mask_path+'/*.png'))
    
    if len(img_files_png) >0:
        solid_color(img_files_png, mask_files_png)
        solid_gradient(img_files_png, mask_files_png)

    if len(img_files_jpg) > 0:
        solid_color(img_files_jpg, mask_files_jpg)
        solid_gradient(img_files_jpg, mask_files_jpg)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--image_path', type=str)
    parser.add_argument('--mask_path', type=str)
    args = parser.parse_args()
    print args.image_path
    print args.mask_path
    main(args)

