import cv2
import random
import numpy as np
import os
from overlay import overlay
from blend_modes import blend_modes

lightBrown = [203, 183, 156]
brown = [117, 61, 33]
lightPurple = [209, 193, 207]
pink=[254, 182, 214]
color_a = [181, 185, 151]
color_b = [53, 96, 200]
color_c = [131, 67, 126]
color_d = [68, 138, 101]
color_e = [161, 211, 60]
color_f = [59, 130, 29]
color_g = [204, 41, 32]
color_h = [221, 130, 39]
color_j = [137, 166, 83]
color_k = [139, 80, 60]

hair_color = (lightBrown, brown, lightPurple, pink, color_g, color_h, color_j, color_k, color_a, color_b, color_c, color_d, color_e, color_f, color_g)

import random
from datetime import datetime


def outPath(path, grad=0):
    output_path_img = os.path.dirname(path)
    out_img = ""
    if grad==0:
        output_img = "solid_color_"+os.path.basename(path)
    elif grad==1:
      output_img = "gradient_color_"+os.path.basename(path)
    elif grad==2:
        output_img = "gamma_adjust_"+os.path.basename(path)

    return os.path.join(output_path_img,output_img)



def solid_color(img_path, mask_path):
    random.seed(datetime.now())
    for img_p, mask_p in zip(img_path, mask_path):
        #load image convert to rgba
        img = cv2.imread(img_p)
        height, width, channels = img.shape
        if channels <3:
            img =cv2.cvtColor(img, cv2.COLOR_GRAY2RGBA)
        else:
            img =cv2.cvtColor(img, cv2.COLOR_RGB2RGBA)
        #load mask
        mask = cv2.imread(mask_p)
        print mask.shape
        if channels ==3:
            print"(asdasdasd"
            mask =cv2.cvtColor(mask, cv2.COLOR_RGB2GRAY)
        if channels ==4:
            print"(asdasdasdasddasadsdas"
            mask =cv2.cvtColor(mask, cv2.COLOR_RGBA2GRAY)
        imgMask = np.zeros((width, height,4), np.uint8)
        color =random.choice(hair_color)
        imgMask[:,:,0] = mask * (color[2]/255.0)
        imgMask[:,:,1] = mask * (color[1]/255.0)
        imgMask[:,:,2] =  mask * (color[0]/255.0)
        imgMask[:,:,3] = mask
        imgMask = imgMask.astype(float)
        img = img.astype(float)
        imgMask = cv2.GaussianBlur(imgMask,(5,5),0)
        imgMask2 = imgMask
        out_img = overlay(img, imgMask, 1)#cv2.add(img, imgMask)
        out_img = overlay(out_img, imgMask2, 1)
        #output stuff
        output_img =outPath(img_p)
        output_mask = outPath(mask_p)

        cv2.imwrite(output_img, out_img)
        cv2.imwrite(output_mask,  mask)


def solid_gradient(img_path, mask_path):
    random.seed(datetime.now())
    for img_p, mask_p in zip(img_path, mask_path):
        #load image convert to rgba
        img = cv2.imread(img_p)
        height, width, channels = img.shape
        if channels <3:
            img =cv2.cvtColor(img, cv2.COLOR_GRAY2RGBA)
        else:
            img =cv2.cvtColor(img, cv2.COLOR_RGB2RGBA)
            #load mask
        mask = cv2.imread(mask_p)
        print mask.shape
        if channels ==3:
            mask =cv2.cvtColor(mask, cv2.COLOR_RGB2GRAY)
        if channels ==4:
                mask =cv2.cvtColor(mask, cv2.COLOR_RGBA2GRAY)
        h_color = (random.choice(hair_color), random.choice(hair_color))
        imgMask = np.zeros((width, height,4), np.uint8)
        for i in range(height):
            ratio = i / float(height)
            grad_1 = np.float_(h_color[0])
            grad_2 = np.float_(h_color[1])
            imgMask[i,:, 3] =  mask[i] * (((grad_1[2] / 255.0) * (1-ratio)) + ((grad_2[2] / 255.0) * (ratio)))
            imgMask[i, :, 1] =  mask[i] * (((grad_1[1] / 255.0) * (1-ratio)) + ((grad_2[1] / 255.0) * (ratio)))
            imgMask[i,:, 2] =  mask[i] * (((grad_1[0] / 255.0) * (1-ratio)) + ((grad_2[0] / 255.0) * (ratio)))


        imgMask[:,:,3] = mask
        imgMask = imgMask.astype(float)
        img = img.astype(float)

        imgMask = cv2.GaussianBlur(imgMask,(5,5),0)
        imgMask2 = imgMask
           #output stuff
        out_img = overlay(img, imgMask, 1)#cv2.add(img, imgMask)
        out_img = overlay(out_img, imgMask2, 1)#cv2.add(img, imgMask)
        output_img =outPath(img_p, 1)
        output_mask = outPath(mask_p, 1)
           
        cv2.imwrite(output_img, out_img)
        cv2.imwrite(output_mask,  mask)








