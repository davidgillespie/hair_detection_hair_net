#!/bin/bash
import cv2
import scipy.stats
import numpy as np

from hair_color import outPath

def gamma_aug(img_path, mask_path):
    lower = 0.5
    upper = 1.5
    mu = 1
    sigma = 0.5
    
    for img_p, mask_p in zip(img_path, mask_path):
        img = cv2.imread(img_p)
        mask = cv2.imread(mask_p)
        alpha = scipy.stats.truncnorm((lower-mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma)
        out_img = (pow(img/255.0, alpha.rvs(1)[0]) * 255).astype(np.uint8)

        output_img =outPath(img_p,2)
        output_mask = outPath(mask_p,2)

        cv2.imwrite(output_img, out_img)
        cv2.imwrite(output_mask,  mask)
