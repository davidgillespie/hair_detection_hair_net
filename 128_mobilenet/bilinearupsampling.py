import tensorflow as tf

from keras.backend import int_shape, permute_dimensions
from keras.layers import *
import numpy as np

def resize_image(x, height_fator, width_factor, data_format):
    """
    Input - tensor, pos int, pos int, dataformat sting (channel first or last)
    returns a tensor
    throws error if data_format is null
        
    """
    if data_format == 'channels_first':
        original_shape = int_shape(x)
        new_shape = tf.shape(x)[2:]
        new_shape *= tf.constant(np.array([height_fator, width_factor]).astype('int32'))
        x = permute_dimensions(x,[0,2,3,1])
        x = tf.image.resize_bilinear(x,new_shape)
        x = permute_dimensions(x,[0,3,1,2])
        s.set_shape((None, None, original_shape[2] * height_fator if original_shape[2] is not None else None,original_shape[1] * width_factor if original_shape[1] is not None else None  ))
        return x
    elif data_format == 'channels_last':
        original_shape = int_shape(x)
        new_shape = tf.shape(x)[1:3]
        new_shape *= tf.constant(np.array([height_fator, width_factor]).astype('int32'))
        x = tf.image.resize_bilinear(x, new_shape)
        x.set_shape((None, original_shape[1]* height_fator if original_shape[1] is not None else None,original_shape[2] * width_factor if original_shape[2] is not None else None, None))
        return x
    else:
        raise ValueError('Invalid data_format:', data_format)


class BilinearUpSampling2D(Layer):
    """
        up sampling layer 2D usin bilinear interpolation
       input = layer
    """

    def __init__(self, size=(2,2), data_format = None, **kwargs):
        super(BilinearUpSampling2D, self).__init__(**kwargs)
        self.data_format = conv_utils.normalize_data_format(data_format)
        self.size = conv_utils.normalize_tuple(size, 2, size)
        self.input_spec = InputSpec(ndim=4)

    def compute_output_shape(self, input_shape):
        if self.data_format =='channels_first': #gray image?
            height = self.size[0] * input_shape[2] if input_shape[2] is not None else None
            width  = self.size[1] * input_shape[3] if input_shape[3] is not None else None
            return (input_shape[0], input_shape[1],  height,width)

        elif self.data_format =='channels_last': #rgb image?
            height = self.size[0] * input_shape[1] if input_shape[1] is not None else None
            width  = self.size[1] * input_shape[2] if input_shape[2] is not None else None
            return (input_shape[0],  height,width, input_shape[3])
    
    def call(self, inputs, **kwargs):
        return resize_image(inputs, self.size[0], self.size[1], self.data_format )

    def get_config(self):
        config= {'size': self.size, 'data_format': self.data_format}
        base_config = super(BilinearUpSampling2D,self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


