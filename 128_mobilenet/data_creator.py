from glob import glob
import os

import cv2

img_files = sorted(glob("dataset/city/train/IMAGE/ori/"+'*.jpg'))
mask_files = sorted(glob("dataset/city/train/GT/mask/"+'*.jpg'))
import numpy as np
x = []
y = []
count = 0
for img_path, mask_path in zip(img_files, mask_files):
    img = cv2.imread(img_path)
    height, width, channel = img.shape
    if channel < 3:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)

    mask = cv2.imread(mask_path)
    height, width, channel = mask.shape
    print count
    count +=1
    if channel < 3:
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
    x.append(img)
    y.append(mask)

    np.save("dataset/images.npy", np.array(x))
    np.save("dataset/masks.npy", np.array(y))



    
