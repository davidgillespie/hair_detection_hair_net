import cv2
import numpy as np
import os
from glob import glob
import argparse
import sys
import Unet_train
import fine_tune
import mobile_net
from Unet_test import *

parser = argparse.ArgumentParser()
parser.add_argument("--data_path", help="training data path", default="./dataset/city")
parser.add_argument("--output_dir", help="output directory", default="./result")
parser.add_argument("--image_size", help="image size", default=128, type=int)
parser.add_argument("--color_mode", help="color", default=0, type=int)
parser.add_argument("--batch_size", help="batch size", default=50, type=int)
parser.add_argument("--total_epoch", help="number of epochs", default=500, type=int)
parser.add_argument("--alpha", help="Alpha for Mob", default=0.5, type=float)
parser.add_argument("--alpha_up", help="Alpha for Mob", default=0.5, type=float)
parser.add_argument("--depth_multiplier", help="Alpha for Mob", default=1, type=int)
parser.add_argument("--initial_learning_rate", help="init lr", default=0.1, type=float)
parser.add_argument("--learning_rate_decay_factor", help="learning rate decay", default=0.05, type=float)
parser.add_argument("--epoch_per_decay", help="lr decay period", default=250, type=int)
parser.add_argument("--ckpt_dir", help="checkpoint root directory", default='./checkpoint')
parser.add_argument("--ckpt_name", help="[.../ckpt_dir/ckpt_name/weights.h5]", default='Unet')
parser.add_argument("--fine_tune_model", help="weight.h5 path", default=None)
parser.add_argument("--confidence_value", help="mask threshold value", default=0.5, type=float)
parser.add_argument("--debug", help="for debug [str: 'true' or 'false']", default='false')
parser.add_argument("--mode",
                    help="[train_full] or [fine_tune] or [mobile_net]",
                    default='train_full')
parser.add_argument("--tf_log_level", help="0, 1, 2, 3", default='3', type=str)

flag = parser.parse_args()

os.environ['TF_CPP_MIN_LOG_LEVEL'] = flag.tf_log_level # or any {'0', '1', '2', '3'}
# os.environ["CUDA_VISIBLE_DEVICES"]="0" 

def main():
    if not os.path.isdir(flag.output_dir):
        os.mkdir(flag.output_dir)
    if flag.mode == 'train_full':
        train_op = Unet_train.TrainModel(flag)
        train_op.train_unet()
    elif flag.mode == 'fine_tune':
        train_op = fine_tune.TrainModel(flag)
        train_op.fine_tune()
    elif flag.mode == 'mobile_net':
        train_op = mobile_net.TrainModel(flag)
        train_op.mobile_net()
    else:
        print 'not supported'

if __name__ == '__main__':
    main()
