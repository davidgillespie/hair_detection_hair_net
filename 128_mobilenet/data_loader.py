from glob import glob
import os

import cv2
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
import random
seed = random.randrange(1, 2000)
from sklearn.model_selection  import train_test_split
img_path = "dataset/images.npy"
mask_path = "dataset/masks.npy"

def create_datagen(images, masks, img_gen, mask_gen, seed):
    print len(images)
    print len(masks)
    
    img_iter = img_gen.flow_from_directory("dataset/city/train/IMAGE",color_mode='rgb', class_mode=None, target_size = (128, 128), seed=seed) #target_size = (128, 128) might need to add this
    print masks[:,:,:,0].shape
    print ("haro")
    mask_iter = mask_gen.flow_from_directory("dataset/city/train/GT", class_mode=None, color_mode='grayscale', target_size = (128, 128),seed=seed )
    # add images to the img_generator
    def datagen():
        while True:
            img = img_iter.next()
            mask = mask_iter.next()
            yield img, mask
    return datagen, len(images)


def load_data():
    images = np.load(img_path)
    masks = np.load(mask_path)

    x_train, x_val, y_train, y_val = train_test_split(images, masks, test_size=0.2, random_state=seed)
    print len(x_train)
    print len(x_val)
    
    print len(y_train)
    print len(y_val)
    train_args = dict(featurewise_center=True,  # set input mean to 0 over the dataset
                 featurewise_std_normalization=True,  # divide inputs by std of the dataset each input by its std
                 zca_whitening=False,  # apply ZCA whitening
                 rotation_range=45,  # randomly rotate images in the range (degrees, 0 to 180)
                 width_shift_range=0.4,  # randomly shift images horizontally (fraction of total width)
                 height_shift_range=0.4,  # randomly shift images vertically (fraction of total height)
                 shear_range=0.4,
                 zoom_range=0.3,
                 horizontal_flip=True,  # randomly flip images
                 vertical_flip=False)
    print ("boom")
    mask_args = dict(
                          rotation_range=45,  # randomly rotate images in the range (degrees, 0 to 180)
                          width_shift_range=0.4,  # randomly shift images horizontally (fraction of total width)
                          height_shift_range=0.4,  # randomly shift images vertically (fraction of total height)
                            rescale = 1. /255,
                          shear_range=0.4,
                          zoom_range=0.3,
                          horizontal_flip=True,  # randomly flip images
                          vertical_flip=False)
    print ("MOOO")
    img_gen = ImageDataGenerator(**train_args)
    
    #  img_gen.fit(images)
    mask_gen =ImageDataGenerator(**mask_args)
    print ("moo")
    train_gen, train_size = create_datagen(x_train, y_train, img_gen, mask_gen, seed)
    
    val_args = dict(featurewise_center=True,  # set input mean to 0 over the dataset

                  featurewise_std_normalization=True,  # divide inputs by std of the dataset

                  zca_whitening=False,  # apply ZCA whitening
                  horizontal_flip=True,  # randomly flip images
                  vertical_flip=False)
    mask_val_args = dict(featurewise_center=False,  # set input mean to 0 over the dataset
                                  samplewise_center=False,  # set each sample mean to 0
                                  featurewise_std_normalization=False,  # divide inputs by std of the dataset
                                  samplewise_std_normalization=False,  # divide each input by its std
                                  zca_whitening=False,  # apply ZCA whitening
                                  horizontal_flip=True,  # randomly flip images
                                  vertical_flip=False,
                         rescale = 1./255)
    val_img_gen = ImageDataGenerator(**val_args)
    #val_img_gen.fit(images)
    val_mask_gen = ImageDataGenerator(**mask_val_args)
    print ("moo 2 ")
    validation_gen, val_size = create_datagen(x_val, y_val, val_img_gen, val_mask_gen, seed)

    return train_gen, validation_gen, images.shape[1:3], train_size, val_size



