from keras.preprocessing.image import ImageDataGenerator
import os
def load_train_data(img_path, img_size, batch_size, seed):
    datagen_args = dict(featurewise_center=True,  # set input mean to 0 over the dataset
                        samplewise_center=False,  # set each sample mean to 0
                        featurewise_std_normalization=True,  # divide inputs by std of the dataset
                        samplewise_std_normalization=False,  # divide each input by its std
                        zca_whitening=False,  # apply ZCA whitening
                        rotation_range=45,  # randomly rotate images in the range (degrees, 0 to 180)
                        width_shift_range=0.4,  # randomly shift images horizontally (fraction of total width)
                        height_shift_range=0.4,  # randomly shift images vertically (fraction of total height)
                        shear_range=0.4,
                        
                        zoom_range=0.3,
                        # preprocessing_function = moo,
                        horizontal_flip=True,  # randomly flip images
                        vertical_flip=False)
    image_datagen = ImageDataGenerator(**datagen_args)
        
    mask_args = dict(
                         rotation_range=45,  # randomly rotate images in the range (degrees, 0 to 180)
                         width_shift_range=0.4,  # randomly shift images horizontally (fraction of total width)
                         height_shift_range=0.4,  # randomly shift images vertically (fraction of total height)
                         rescale = 1. /255,
                         shear_range=0.4,
                         zoom_range=0.3,
                         horizontal_flip=True,  # randomly flip images
                         vertical_flip=False)
    mask_datagen = ImageDataGenerator(**datagen_args)

    image_generator = image_datagen.flow_from_directory( os.path.join(img_path, 'train/IMAGE'),
                                                                             class_mode=None, seed=seed, batch_size=batch_size, color_mode='rgb', target_size = (img_size, img_size))
    mask_generator = mask_datagen.flow_from_directory(
    os.path.join(img_path, 'train/GT'),class_mode=None, seed=seed, batch_size=batch_size, color_mode='grayscale', target_size = (img_size, img_size))

    return image_generator, mask_generator


